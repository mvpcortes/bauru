# bauru

# BAURU 

Engine for 2D RPG games in typescript and PIXI.js

# Used
- We use template for PIXI.js from: https://github.com/llorenspujol/parcel-pixijs-quickstarter

## We can read Tiled files?
 - https://github.com/riebel/pixi-tiledmap
 - https://github.com/eioo/pixi-tiledmap

### Tiles?
 - https://github.com/pixijs/pixi-tilemap
