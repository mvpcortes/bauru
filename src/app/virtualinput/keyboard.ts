import { Closeable } from "../Closeable";

/**
 * This class is derived from pixijs tutorial: https://github.com/kittykatattack/learningPixi#keyboard-movement
 * 
 */
 export class KeyboardSubscriber implements Closeable{
     private value: string;
     private isDown= false;
     private isUp = true;
     public press = undefined;
     public release = undefined;

    constructor(v:string) {
        this.value = v;
        window.addEventListener("keydown", this.downHandler, false);
        window.addEventListener("keyup", this.upHandler, false);
          
     }

     close(): void {
        window.removeEventListener("keydown", this.downHandler)
        window.removeEventListener("keyup",  this.upHandler)
     }

     protected downHandler(event:KeyboardEvent):void {
        if (event.key === this.value) {
            if (this.isUp && this.press) this.press();
            this.isDown = true;
            this.isUp = false;
            event.preventDefault();
        }
     }

     protected upHandler(event:KeyboardEvent):void {
        if (event.key === this.value) {
            if (this.isDown && this.release) this.release();
            this.isDown = false;
            this.isUp = true;
            event.preventDefault();
        }
     }
 }
    
    // // Detach event listeners
    
