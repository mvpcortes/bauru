import { IWindow } from "../base";
import { Closeable } from "../Closeable";
/**
 * Virtual Input 
 * We create a virtual input (joystick, joypad, keyboard, etc) and connect real input events to
 * virtual input events.
 * 
 * The virtual events are:
 * * UP     : up pad
 * * DOWN   : down pad
 * * LEFT   : left pad
 * * RIGHT  : right pad
 * * A      : A button - ACTION                                 : ex. Keyboard.X (like RPGMaker)
 * * B      : B button - OPTION / MENU                          : ex. Keyboard.Y (like RPGMaker)
 * * CTRL   : L button - LEFT TAB in menus and extra actions    : ex. Keyboard.Control
 * * SHIFT  : R button - RIGHT TAB in menus and extra actions   : ex. Keyboard.Shift
 * * PAUSE  : PAUSE button - pause the game                     : ex. Keyboard.ENTER
 * * EXIT   : EXIT button - exit the game                       : ex. Keyboard.ESC
 */

import { KeyboardSubscriber } from "./keyboard";


 export class DEFAULT_PRIORITIES {
     public static readonly LOWER_PRIORITY = Number.MIN_VALUE;
     public static readonly SCENE_PRIORITY = 0;
     public static readonly HERO_PRIORITY = 1;
     public static readonly MENU_PRIORITY = 2;
     public static readonly HIGHER_PRIORITY = Number.MAX_VALUE;
 }

 export enum VIRTUAL_KEY {
     UP,
     DOWN,
     LEFT,
     RIGHT,
     A,
     B,
     L,
     R,
     PAUSE,
     EXIT
 }

 export interface KeyListener {

    /**
     * The virtual key is pressed
     * @returns true if deal with event and does not call after listener. false indead.
     */
    press(event:VIRTUAL_KEY):boolean

    /**
     * The virtual key is released
     * @returns true if deal with event and does not call after listener. false indead.
     */
    release(event: VIRTUAL_KEY):boolean
 }

 interface IVirtualKeyController {
       subscribe: (name:String, priority:Number, listener:KeyListener)=>void;
     unsubscribe: (name:String)=>void;
 }

 
interface ControlledVirtualKeyListener {
    
    name:string;
    priority: number;
    listener: KeyListener;
}

// class Queue<T extends ControlledListener> {
//      internalArray: Array<T>;

//      push(t:T):void{
//          this.internalArray.push(t)
//          this.internalArray.sort((a, b)=>(a.priority-b.priority))
//      }

//      remove(t:T):boolean{
//         const index = this.internalArray.indexOf(t, 0);
//         if (index > -1) {
//             this.internalArray.splice(index, 1);
//             return true;
//         }
//         return false;
//      }
//  }

class KeyState{
    public readonly virtualKey: VIRTUAL_KEY;
    public isDown= false;
    public isUp = true;

    constructor(virtualKey:VIRTUAL_KEY){
        this.virtualKey=virtualKey
    }
}

 
/**
 * Listener the window browser class and delegate to functions
 */
 class KeyboardBrowserProxy implements Closeable {
    
     private readonly mapKeys = new Map<String, KeyState>()

     private readonly controller:VirtualKeyController

     private readonly window:Window|IWindow

     constructor(controller:VirtualKeyController, window:IWindow|Window){
        
        //
        this.controller = controller
        this.window = window

        this.mapKeys.set("ArrowLeft", new KeyState(VIRTUAL_KEY.LEFT));
        this.mapKeys.set("ArrowRight", new KeyState(VIRTUAL_KEY.RIGHT));
        this.mapKeys.set("ArrowUp", new KeyState(VIRTUAL_KEY.UP));
        this.mapKeys.set("ArrowDown", new KeyState(VIRTUAL_KEY.DOWN));
        this.mapKeys.set("z", new KeyState(VIRTUAL_KEY.A));
        this.mapKeys.set("x", new KeyState(VIRTUAL_KEY.B));
        this.mapKeys.set("Control", new KeyState(VIRTUAL_KEY.L));
        this.mapKeys.set("Shift", new KeyState(VIRTUAL_KEY.R));
        this.mapKeys.set("Enter", new KeyState(VIRTUAL_KEY.PAUSE));
        this.mapKeys.set("Escape", new KeyState(VIRTUAL_KEY.EXIT));

        //init listener on window
        console.info("começando a adicionar eventos")
        var _self = this
        this.window.addEventListener("keydown", (e:KeyboardEvent)=>_self.downHandler(e), false);
        this.window.addEventListener("keyup", (e:KeyboardEvent)=>_self.upHandler(e), false);
          
     }

     close(): void {
        this.window.removeEventListener("keydown", this.downHandler)
        this.window.removeEventListener("keyup",  this.upHandler)
     }

     protected downHandler(event:KeyboardEvent):void {
        let value:KeyState = this.mapKeys.get(event.key)
        if(value?true:false){
            if(value.isUp)this.controller.press(value.virtualKey)
            value.isUp = false
            value.isDown = true
            event.preventDefault()
        }
     }

     protected upHandler(event:KeyboardEvent):void {
        let value:KeyState = this.mapKeys.get(event.key)
        if(value?true:false){
            if(value.isDown)this.controller.release(value.virtualKey)
            value.isUp = true
            value.isDown = false
            event.preventDefault()
        }
     }
 }

 export class ProgrammmingProxy {
     private readonly controller: VirtualKeyController;

     constructor(controller:VirtualKeyController){
         this.controller=controller;
     }

     public press(key:VIRTUAL_KEY):void{
         this.controller.press(key)
     }

     public release(key:VIRTUAL_KEY):void{
        this.controller.release(key)
    }
 }
 

 export class VirtualKeyController implements IVirtualKeyController, Closeable {

    private queueListener:Array<ControlledVirtualKeyListener> = [];

    private proxies:Closeable[]=[]

    constructor(_window: Window | IWindow){
        //keyboard
        //add this proxies only window var is found
        if(_window?true:false){
            this.proxies.push(new KeyboardBrowserProxy(this, _window))
        }
    }

    close():void {
        this.proxies.forEach(proxy=>{proxy.close()})
    }
    
     
    public subscribe(name: string, priority: number, listener: KeyListener) : void {
        this.queueListener.push({name:name, priority:priority, listener:listener})
        this.queueListener.sort((a, b)=>{return b.priority-a.priority;})
    }

    public unsubscribe(name:string) : void {
        this.queueListener = this.queueListener.filter(e=>e.name!==name)
    }

    public press(vk:VIRTUAL_KEY):void {
       for(let i = 0; i < this.queueListener.length; i++){
           let listener = this.queueListener[i]
           if(listener.listener.press(vk)){
               break;
           }
       }
    }

    public release(vk:VIRTUAL_KEY):void {
        for(let i = 0; i < this.queueListener.length; i++){
            let listener = this.queueListener[i]
            if(listener.listener.release(vk)){
                break;
            }
        }
    }

    public createProgrammingProxy():ProgrammmingProxy{
        return new ProgrammmingProxy(this);
    }

    public getListeners():Array<KeyListener>{
        return this.queueListener.map(x=>x.listener)
    }

 }