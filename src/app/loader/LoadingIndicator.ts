import { Graphics, Loader } from "pixi.js";

/**
 * Stage Layer to show loading information
 */
export class LoadingIndicator {

    // private mapLoaders = new Map<Loader, number>()

    public graphics
    private pct = 0
    public barWidth:number=200
    public barHeight:number=20


    public constructor(loader:Loader, graphics?:Graphics){
        
        if(!graphics){
            this.graphics = new Graphics()
        }
    
        this.graphics = graphics
        this.graphics.visible  = true
        this.graphics.zIndex   =   10


        //start loader process (pre all resources)
        loader.onStart      .add((loader)=>this.doUpdate(0, "start"))

        //pos load resource
        loader.onProgress   .add((loader, resource)=>this.doUpdate(loader.progress, "progress"))

        //pos load resource
        loader.onLoad       .add((loader, resource)=>this.doUpdate(loader.progress, "load"))

        //pos load resource
        loader.onError      .add((loader, resource)=>this.doUpdate(loader.progress, "error"))

        //end loader process
        loader.onComplete   .add((loader,resources)=>this.doUpdate(100, "complete"))
    }


    public ratioFn(qtd:number, total:number):number{return (total >0)?((qtd/total)):1}

    private doUpdate(pct:number, tipo:string){
        console.log("doUpdate: " + pct + ", " + tipo)
        this.pct = pct
        this.update()
    }
    public update() {
        let qtd = this.pct
        let total = 100
        let ratio = this.ratioFn(qtd, total)
        console.log("QTD=%s, TOTAL=%s, pct=%", qtd, total, ratio*100)

        const fivePecent = Math.max(this.barHeight*5/100, 1)

        let widthFilled = (this.barWidth-fivePecent)*ratio

        this.graphics.beginFill(0x000000)
            this.graphics.drawRect(0,0, this.barWidth, this.barHeight)
        this.graphics.endFill()

        this.graphics.beginFill(0x1eFF00)
            this.graphics.drawRect(fivePecent,fivePecent, widthFilled, this.barHeight-fivePecent)
        this.graphics.endFill()

        this.graphics.x=0
        this.graphics.y=0
    }
}