/**
 * Fake Windows is used to simulate key pess on window browser
 */
import { IWindow } from "../../base"
import { mock } from "jest-mock-extended"


 export class FakeWindow implements IWindow {

     private listeners = new Map<string, Array<EventListener>>()

     constructor(){
         this.listeners.set("keyup", [])
         this.listeners.set("keydown", [])
     }

    public addEventListener(type: string, listener: EventListener, options?: boolean | AddEventListenerOptions): void{
        let list = this.listeners.get(type)
        
        if(list?true:false){
            list = []
            this.listeners.set(type, list)
        }

        list.push(listener)
    }

    public removeEventListener(type: string, listener: EventListener | EventListenerObject, options?: boolean | EventListenerOptions): void{
        let list = this.listeners.get(type)
        if(list?true:false){
            list = list.filter(x=>x!==listener)
        }
    }

    public press(key:string):void {

        console.info("press window: %s - handlers: %s", key, this.listeners)
        this.listeners.get("keydown").forEach(listener=>listener(this.createEvent(key)))
    }

    public release(key:string):void {

        console.info("release window: %s - handlers: %s", key, this.listeners)
        this.listeners.get("keyup").forEach(listener=>listener(this.createEvent(key)))
    }

    private createEvent(key:string) : KeyboardEvent{
        return mock<KeyboardEvent>({key:key})
    }


 }