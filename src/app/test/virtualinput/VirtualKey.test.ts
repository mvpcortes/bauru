import { DEFAULT_PRIORITIES, KeyListener, ProgrammmingProxy, VirtualKeyController, VIRTUAL_KEY } 
from "../../virtualinput/VirtualKey";
import { mock } from 'jest-mock-extended';
import { FakeWindow } from "./FakeWindow";


describe("A VirtualKeyController", ()=>{

    var window = new FakeWindow()

    var virtualKeyController:VirtualKeyController
    var programmingProxy:ProgrammmingProxy
    var keyListener:KeyListener


    beforeEach(()=>{
        virtualKeyController= new VirtualKeyController(window)
        programmingProxy=virtualKeyController.createProgrammingProxy()
        keyListener= mock<KeyListener>()//new MockKeyListener();
        virtualKeyController.subscribe("my_listener", DEFAULT_PRIORITIES.HIGHER_PRIORITY, keyListener)
    })

    afterEach(()=>{
        virtualKeyController.unsubscribe("my_listener")
        virtualKeyController.close()
        virtualKeyController=null
    })

    test("when push a lot of listeners then order is keeped", ()=>{
        let L_1 = mock<KeyListener>()
        let L_2 = mock<KeyListener>()
        let L_3 = mock<KeyListener>()

        virtualKeyController.subscribe("L_2", 2, L_2)
        virtualKeyController.subscribe("L_3", 3, L_3)
        virtualKeyController.subscribe("L_1", 1, L_1)

        let lll = virtualKeyController.getListeners();

        expect(lll).toHaveLength(4)
        expect(lll[0]).toBe(keyListener)
        expect(lll[1]).toBe(L_3)
        expect(lll[2]).toBe(L_2)
        expect(lll[3]).toBe(L_1)

        virtualKeyController.unsubscribe("L_1")
        virtualKeyController.unsubscribe("L_2")
        virtualKeyController.unsubscribe("L_3")

    })

    test("when key A is pressed by a source then send to keyListener", ()=>{

        programmingProxy.press(VIRTUAL_KEY.A)

        expect(keyListener.press).toBeCalledTimes(1)
        expect(keyListener.press).toBeCalledWith(VIRTUAL_KEY.A)
    })

    test("when key B is released by a source then send to keyListener", ()=>{

        programmingProxy.release(VIRTUAL_KEY.A)

        expect(keyListener.release).toBeCalledTimes(1)
        expect(keyListener.release).toBeCalledWith(VIRTUAL_KEY.A)
    })

    describe.each([
        Object.keys(VIRTUAL_KEY)
            .filter(name=>typeof VIRTUAL_KEY[name] == 'number')    
            .map(name=>VIRTUAL_KEY[name])
    ])("The %s key", (key)=>{
            test('when key ${key} is pressed by a source then send to keyListener', ()=>{           
                programmingProxy.press(key)
                expect(keyListener.press).toBeCalledTimes(1)
                expect(keyListener.press).toBeCalledWith(key)
            })

            test("when key ${key} is released by a source then send to keyListener", ()=>{           
                programmingProxy.release(key)
                expect(keyListener.release).toBeCalledTimes(1)
                expect(keyListener.release).toBeCalledWith(key)
            })
    })

    describe("Window key press", ()=>{

        test("when window press x key then controller propagate to listener with B virtual key", ()=>{
            window.press("x")

            expect(keyListener.press).toBeCalledTimes(1)
            expect(keyListener.press).toBeCalledWith(VIRTUAL_KEY.B)
        })

        test("when window press x key then controller propagate to listener with RIGHGT virtual key", ()=>{
            window.press("Shift")

            expect(keyListener.press).toBeCalledTimes(1)
            expect(keyListener.press).toBeCalledWith(VIRTUAL_KEY.R)
        })
    })
    
})