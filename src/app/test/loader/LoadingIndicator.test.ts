import { mock, MockProxy } from "jest-mock-extended";
import { LoadingIndicator } from "../../loader/LoadingIndicator"

import {Loader, Graphics} from 'pixi.js'

describe("A VirtualKeyController", ()=>{

    var loadingIndicator:LoadingIndicator
    var graphics: MockProxy<Graphics> & Graphics
    var loader:Loader

    beforeEach(()=>{

        loader = new Loader("")
        // Chainable `add` to enqueue a resource
        .add("a")
        .add("b")
        .add("c")
        .use((resource, next) =>
        {
            // console.info(resource)
            // Be sure to call next() when you have completed your middleware work.
            next();
        })
        // .load((loader, resources) => {
        //     //do nothing :c)
        // });

        graphics = mock<Graphics>()//new Graphics()

        loadingIndicator = new LoadingIndicator(loader, graphics)

    })
    
    /**
     * @jest-environment jsdom
     */
     test("When loader Resources then call LoadingIndicator", ()=>{
         
        //const result = //await loader.load((resource, next)=>{/** Do nothing :c) */})

        loader.load((resource, next)=>{
            expect(graphics.beginFill).toHaveBeenNthCalledWith(1, 0x000000);
            expect(graphics.drawRect)
                .toHaveBeenNthCalledWith(2, 0, 0, loadingIndicator.barWidth, loadingIndicator.barHeight);
            expect(graphics.endFill).toHaveBeenNthCalledWith(1);
            expect(graphics.beginFill).toHaveBeenNthCalledWith(2, 0x1eFF00);
            expect(graphics.drawRect)
                .toHaveBeenNthCalledWith(2, 1, 1, 0, 19);
            expect(graphics.endFill).toHaveBeenNthCalledWith(2);

            expect(graphics.beginFill).toHaveBeenCalledTimes(2)
            expect(graphics.drawRect).toHaveBeenCalledTimes(2)
            expect(graphics.endFill).toHaveBeenCalledTimes(2)
            
            
            // expect(graphics.beginFill).toHaveBeenNthCalledWith(3, 0x000000);
            // expect(graphics.beginFill).toHaveBeenNthCalledWith(4, 0x1eFF00);
            
            // expect(graphics.beginFill).toHaveBeenCalledWith(0x000000)
            // expect(graphics.beginFill).toHaveBeenCalledWith(0x1eFF00)
            // expect(graphics.beginFill).toHaveBeenCalledWith(0x000000)
            // expect(graphics.beginFill).toHaveBeenCalledWith(0x1eFF00)
            // expect(graphics.beginFill).toHaveBeenCalledWith(0x000000)
            // expect(graphics.beginFill).toHaveBeenCalledWith(0x1eFF00)

            
            // this.graphics.beginFill(0x000000)
            // this.graphics.drawRect(0,0, this.barWidth, this.barHeight)
            //     this.graphics.endFill()

            //     this.graphics.beginFill(0x1eFF00)
            //         this.graphics.drawRect(fivePecent,fivePecent, widthFilled, this.barHeight-fivePecent)
            //     this.graphics.endFill()

            //     this.graphics.x=0
            //     this.graphics.y=0
        })
    })    
})